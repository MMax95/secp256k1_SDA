#ifndef SECP251K1_SANDBOX_NETWORKMANAGER_H
#define SECP251K1_SANDBOX_NETWORKMANAGER_H
#include "../../src/headers/Operators.h"

int preparePackage();
int connectToHost();
int sendToHost();

#endif // SECP251K1_SANDBOX_NETWORKMANAGER_H